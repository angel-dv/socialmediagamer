package com.devfg.socialmediagamer.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devfg.socialmediagamer.R;
import com.devfg.socialmediagamer.models.Message;
import com.devfg.socialmediagamer.providers.AuthProvider;
import com.devfg.socialmediagamer.providers.UserProviders;
import com.devfg.socialmediagamer.utils.RelativeTime;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

public class MessageAdapter extends FirestoreRecyclerAdapter<Message, MessageAdapter.ViewHolder> {

    Context context;
    UserProviders userProviders;
    AuthProvider authProvider;

    public MessageAdapter(FirestoreRecyclerOptions<Message> options, Context context) {
        super(options);
        this.context = context;
        userProviders = new UserProviders();
        authProvider = new AuthProvider();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull final Message message) {
        DocumentSnapshot document = getSnapshots().getSnapshot(position);
        final String messageId = document.getId();

        holder.tvMessage.setText(message.getMessage());

        String relativeTime = RelativeTime.timeFormatAMPM(message.getTimestamp(), context);
        holder.tvDate.setText(relativeTime);

        if(message.getIdSender().equals(authProvider.getUid())) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
            );
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.setMargins(150, 0, 0, 0);
            holder.llMessage.setLayoutParams(params);
            holder.llMessage.setPadding(30, 20, 0, 20);
            holder.llMessage.setBackground(context.getResources().getDrawable(R.drawable.roundend_linear_layout));
            holder.ivViewed.setVisibility(View.VISIBLE);
            holder.tvMessage.setTextColor(Color.WHITE);
            holder.tvDate.setTextColor(Color.LTGRAY);

        } else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                    );
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.setMargins(0, 0, 150, 0);
            holder.llMessage.setLayoutParams(params);
            holder.llMessage.setPadding(30, 20, 30, 20);
            holder.llMessage.setBackground(context.getResources().getDrawable(R.drawable.roundend_linear_layout_grey));
            holder.ivViewed.setVisibility(View.GONE);
            holder.tvMessage.setTextColor(Color.DKGRAY);
            holder.tvDate.setTextColor(Color.LTGRAY);
        }

        if (message.isViewed()) {
            holder.ivViewed.setImageResource(R.drawable.icon_check_blue);
        } else {
            holder.ivViewed.setImageResource(R.drawable.icon_check_gray);
        }

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_message, parent, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvMessage, tvDate;
        ImageView ivViewed;
        LinearLayout llMessage;
        View viewHolder;

        public ViewHolder(View v) {
            super(v);
            tvMessage = v.findViewById(R.id.textViewMessage);
            tvDate = v.findViewById(R.id.textViewDateMessage);
            ivViewed = v.findViewById(R.id.imageViewViewdMessage);
            llMessage = v.findViewById(R.id.linearLayoutMessage);
            viewHolder = v;
        }
    }
}
