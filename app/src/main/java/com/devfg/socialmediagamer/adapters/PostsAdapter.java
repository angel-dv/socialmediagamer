package com.devfg.socialmediagamer.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.devfg.socialmediagamer.R;
import com.devfg.socialmediagamer.activities.PostDetailActivity;
import com.devfg.socialmediagamer.models.Like;
import com.devfg.socialmediagamer.models.Post;
import com.devfg.socialmediagamer.providers.AuthProvider;
import com.devfg.socialmediagamer.providers.LikesProvider;
import com.devfg.socialmediagamer.providers.UserProviders;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.Date;

public class PostsAdapter extends FirestoreRecyclerAdapter<Post, PostsAdapter.ViewHolder> {

    Context context;
    UserProviders userProviders;
    LikesProvider likesProvider;
    AuthProvider authProvider;
    TextView tvNumberFilter;
    ListenerRegistration mListener;

    public PostsAdapter(FirestoreRecyclerOptions<Post> options, Context context) {
        super(options);
        this.context = context;
        userProviders = new UserProviders();
        likesProvider = new LikesProvider();
        authProvider = new AuthProvider();
    }

    public PostsAdapter(FirestoreRecyclerOptions<Post> options, Context context, TextView textView) {
        super(options);
        this.context = context;
        userProviders = new UserProviders();
        likesProvider = new LikesProvider();
        authProvider = new AuthProvider();
        tvNumberFilter = textView;
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder holder, int position, @NonNull final Post post) {
        DocumentSnapshot document = getSnapshots().getSnapshot(position);
        final String postId = document.getId();
//        String userId = document.getString("idUser");
        if (tvNumberFilter != null) {
            int numberFilter = getSnapshots().size();
            tvNumberFilter.setText(String.valueOf(numberFilter));
        }

        holder.tvTitle.setText(post.getTitle().toUpperCase());
        holder.tvDescription.setText(post.getDescription());

        if (post.getImage1() != null) {
            if (!post.getImage1().isEmpty()) {
                Picasso.with(context).load(post.getImage1()).into(holder.ivPost);
            }
        }

        holder.viewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PostDetailActivity.class);
                i.putExtra("id", postId);
                context.startActivity(i);
            }
        });

        holder.ivLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Like like = new Like();
                like.setIdUser(authProvider.getUid());
                like.setIdPost(postId);
                like.setTimestamp(new Date().getTime());
                like(like, holder);
            }
        });

        getUserInfo(post.getIdUser(), holder);
        getNumberLikesByPost(postId, holder);
        checkIfExistLike(postId, authProvider.getUid(), holder);
    }

    private void getNumberLikesByPost(String idPost, final ViewHolder holder) {
        mListener = likesProvider.getLikesByPost(idPost).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(queryDocumentSnapshots != null) {
                    int numberLikes = queryDocumentSnapshots.size();
                    holder.tvLikes.setText(String.valueOf(numberLikes) + " Me gustas");
                }
            }
        });
    }

    private void like(final Like like, final ViewHolder holder) {
        likesProvider.getLikeByPostAndUser(like.getIdPost(), authProvider.getUid()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                int numberDocument = queryDocumentSnapshots.size();
                if (numberDocument > 0) {
                    String idLike = queryDocumentSnapshots.getDocuments().get(0).getId();
                    holder.ivLikes.setImageResource(R.drawable.icon_like_gray);
                    likesProvider.delete(idLike);
                } else {
                    holder.ivLikes.setImageResource(R.drawable.icon_like_blue);
                    likesProvider.create(like);
                }
            }
        });
    }

    private void checkIfExistLike(String idPost, String idUser, final ViewHolder holder) {
        likesProvider.getLikeByPostAndUser(idPost, idUser).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                int numberDocument = queryDocumentSnapshots.size();
                if (numberDocument > 0) {
                    holder.ivLikes.setImageResource(R.drawable.icon_like_blue);
                } else {
                    holder.ivLikes.setImageResource(R.drawable.icon_like_gray);
                }
            }
        });
    }

    private void getUserInfo(String idUser, final ViewHolder holder) {
        userProviders.getUser(idUser).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    if (documentSnapshot.contains("username")) {
                        String username = documentSnapshot.getString("username");
                        holder.tvUsername.setText(String.format("By: %s", username.toUpperCase()));
                    }
                }
            }
        });
    }

    public ListenerRegistration getListener() {
        return mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_post, parent, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDescription, tvUsername, tvLikes;
        ImageView ivPost, ivLikes;
        View viewHolder;

        public ViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.textViewTitlePostCard);
            tvDescription = v.findViewById(R.id.textViewDescriptionPostCard);
            tvUsername = v.findViewById(R.id.textViewUsernamePostCard);
            tvLikes = v.findViewById(R.id.textViewLikes);
            ivLikes = v.findViewById(R.id.imageViewLike);
            ivPost = v.findViewById(R.id.imageViewPostCard);
            viewHolder = v;
        }
    }
}
