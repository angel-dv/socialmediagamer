package com.devfg.socialmediagamer.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.devfg.socialmediagamer.R;
import com.devfg.socialmediagamer.activities.ChatActivity;
import com.devfg.socialmediagamer.models.Chat;
import com.devfg.socialmediagamer.providers.AuthProvider;
import com.devfg.socialmediagamer.providers.ChatsProvider;
import com.devfg.socialmediagamer.providers.MessageProvider;
import com.devfg.socialmediagamer.providers.UserProviders;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatsAdapter extends FirestoreRecyclerAdapter<Chat, ChatsAdapter.ViewHolder> {

    Context context;
    UserProviders userProviders;
    AuthProvider authProvider;
    ChatsProvider chatsProvider;
    MessageProvider messageProvider;
    ListenerRegistration mListener, mListenerLastMessage;

    public ChatsAdapter(FirestoreRecyclerOptions<Chat> options, Context context) {
        super(options);
        this.context = context;
        userProviders = new UserProviders();
        authProvider = new AuthProvider();
        chatsProvider = new ChatsProvider();
        messageProvider = new MessageProvider();
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull final Chat chat) {
        DocumentSnapshot document = getSnapshots().getSnapshot(position);
        final String chatId = document.getId();

        if(authProvider.getUid().equals(chat.getIdUser1())) {
            getUserInfo(chat.getIdUser2(), holder);
        } else {
            getUserInfo(chat.getIdUser1(), holder);
        }

        holder.viewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToChatActivity(chatId, chat.getIdUser1(), chat.getIdUser2());
            }
        });

        getLastMessage(chatId, holder.tvLastMessage);

        String idSender = "";
        if (authProvider.getUid().equals(chat.getIdUser1())) {
            idSender = chat.getIdUser2();
        } else {
            idSender = chat.getIdUser1();
        }
        getMessageNotRead(chatId, idSender, holder.tvMessageNotRead, holder.flMessageNotRead);
    }

    private void getMessageNotRead(String chatId, String idSender, final TextView tvMessageNotRead, final FrameLayout flMessageNotRead) {
        mListener = messageProvider.getMessageByChatAndSender(chatId, idSender).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    int size = queryDocumentSnapshots.size();
                    if (size > 0) {
                        flMessageNotRead.setVisibility(View.VISIBLE);
                        tvMessageNotRead.setText(String.valueOf(size));
                    } else {
                        flMessageNotRead.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    public ListenerRegistration getListener() {
        return mListener;
    }

    public ListenerRegistration getListenerLastMessage() {
        return mListenerLastMessage;
    }

    private void getLastMessage(String chatId, final TextView tvLastMessage) {
        mListenerLastMessage = messageProvider.getLastMessage(chatId).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    int size = queryDocumentSnapshots.size();
                    if (size > 0) {
                        String lastMessage = queryDocumentSnapshots.getDocuments().get(0).getString("message");
                        tvLastMessage.setText(lastMessage);
                    }
                }
            }
        });
    }

    private void goToChatActivity(String chatId, String idUser1, String idUser2) {
        Intent i = new Intent(context, ChatActivity.class);
        i.putExtra("idChat", chatId);
        i.putExtra("idUser1", idUser1);
        i.putExtra("idUser2", idUser2);
        context.startActivity(i);
    }

    private void getUserInfo(String idUser, final ViewHolder holder) {
        userProviders.getUser(idUser).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    if (documentSnapshot.contains("username")) {
                        String username = documentSnapshot.getString("username");
                        holder.tvUsername.setText(username.toUpperCase());
                    }
                    if (documentSnapshot.contains("image_profile")) {
                        String imageProfile = documentSnapshot.getString("image_profile");
                        if (imageProfile != null) {
                            if (!imageProfile.isEmpty()) {
                                Picasso.with(context).load(imageProfile).into(holder.civChat);
                            }
                        }
                    }
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_chat, parent, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvUsername, tvLastMessage, tvMessageNotRead;
        CircleImageView civChat;
        FrameLayout flMessageNotRead;
        View viewHolder;

        public ViewHolder(View v) {
            super(v);
            tvUsername = v.findViewById(R.id.textViewUsernameChat);
            tvLastMessage = v.findViewById(R.id.textViewLastMessageChat);
            tvMessageNotRead = v.findViewById(R.id.textViewMessageNotRead);
            flMessageNotRead = v.findViewById(R.id.frameLayoutMessageNotRead);
            civChat = v.findViewById(R.id.circleImageViewChat);
            viewHolder = v;
        }
    }
}
