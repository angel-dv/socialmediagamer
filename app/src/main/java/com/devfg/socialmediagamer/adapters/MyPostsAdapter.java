package com.devfg.socialmediagamer.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devfg.socialmediagamer.R;
import com.devfg.socialmediagamer.activities.PostDetailActivity;
import com.devfg.socialmediagamer.models.Post;
import com.devfg.socialmediagamer.providers.AuthProvider;
import com.devfg.socialmediagamer.providers.LikesProvider;
import com.devfg.socialmediagamer.providers.PostProvider;
import com.devfg.socialmediagamer.providers.UserProviders;
import com.devfg.socialmediagamer.utils.RelativeTime;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyPostsAdapter extends FirestoreRecyclerAdapter<Post, MyPostsAdapter.ViewHolder> {

    Context context;
    UserProviders userProviders;
    LikesProvider likesProvider;
    AuthProvider authProvider;
    PostProvider postProvider;

    public MyPostsAdapter(FirestoreRecyclerOptions<Post> options, Context context) {
        super(options);
        this.context = context;
        userProviders = new UserProviders();
        likesProvider = new LikesProvider();
        authProvider = new AuthProvider();
        postProvider = new PostProvider();
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewHolder holder, int position, @NonNull final Post post) {
        DocumentSnapshot document = getSnapshots().getSnapshot(position);
        final String postId = document.getId();
        String relativeTime = RelativeTime.getTimeAgo(post.getTimestamp(), context);

        if(post.getIdUser().equals(authProvider.getUid())) {
            holder.ivDeleteMyPost.setVisibility(View.VISIBLE);
        } else {
            holder.ivDeleteMyPost.setVisibility(View.GONE);
        }

        holder.tvTitle.setText(post.getTitle().toUpperCase());
        holder.tvRelativeTime.setText(relativeTime);

        if (post.getImage1() != null) {
            if (!post.getImage1().isEmpty()) {
                Picasso.with(context).load(post.getImage1()).into(holder.civMyPost);
            }
        }

        holder.viewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PostDetailActivity.class);
                i.putExtra("id", postId);
                context.startActivity(i);
            }
        });

        holder.ivDeleteMyPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmDelete(postId);
            }
        });

    }

    private void showConfirmDelete(final String postId) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Eliminar publicacion")
                .setMessage("¿Estas seguro de realizar esta acción?")
                .setPositiveButton("SI", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deletePost(postId);
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    private void deletePost(String postId) {
        postProvider.delete(postId).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(context, "El post se elimino correctamente", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "No se elimino el post", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_my_post, parent, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvRelativeTime;
        CircleImageView civMyPost;
        ImageView ivDeleteMyPost;
        View viewHolder;

        public ViewHolder(View v) {
            super(v);
            tvTitle = v.findViewById(R.id.textViewTitleMyPost);
            tvRelativeTime = v.findViewById(R.id.textViewRelativeTimeMyPost);
            civMyPost = v.findViewById(R.id.circleImageViewMyPost);
            ivDeleteMyPost = v.findViewById(R.id.imageViewDeleteMyPost);
            viewHolder = v;
        }
    }
}
