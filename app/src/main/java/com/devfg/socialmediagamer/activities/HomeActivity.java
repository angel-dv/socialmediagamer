package com.devfg.socialmediagamer.activities;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.devfg.socialmediagamer.R;
import com.devfg.socialmediagamer.fragments.ChatsFragment;
import com.devfg.socialmediagamer.fragments.FilterFragment;
import com.devfg.socialmediagamer.fragments.HomeFragment;
import com.devfg.socialmediagamer.fragments.ProfileFragment;
import com.devfg.socialmediagamer.providers.AuthProvider;
import com.devfg.socialmediagamer.providers.TokenProvider;
import com.devfg.socialmediagamer.providers.UserProviders;
import com.devfg.socialmediagamer.utils.ViewedMessageHelper;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigation;

    TokenProvider mTokenProvider;
    AuthProvider mAuthProvider;
    UserProviders userProviders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        mTokenProvider = new TokenProvider();
        mAuthProvider = new AuthProvider();
        userProviders = new UserProviders();
        openFragment(new HomeFragment());

        createToken();
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        updateOnline(false);
//    }

    @Override
    protected void onStart() {
        super.onStart();
//        updateOnline(true);
        ViewedMessageHelper.updateOnline(true, HomeActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ViewedMessageHelper.updateOnline(false, HomeActivity.this);
    }

//    private void updateOnline(boolean status) {
//        userProviders.updateOnline(mAuthProvider.getUid(), status);
//    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    if (item.getItemId() == R.id.itemHome) {
                        openFragment(new HomeFragment());
                    } else if (item.getItemId() == R.id.itemChats) {
                        openFragment(new ChatsFragment());
                    } else if (item.getItemId() == R.id.itemFilters) {
                        openFragment(new FilterFragment());
                    } else if (item.getItemId() == R.id.itemProfile) {
                        openFragment(new ProfileFragment());
                    }
                    return true;
                }
            };

    private void createToken() {
        mTokenProvider.create(mAuthProvider.getUid());
    }
}