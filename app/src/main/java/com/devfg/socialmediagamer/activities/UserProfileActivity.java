package com.devfg.socialmediagamer.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.devfg.socialmediagamer.R;
import com.devfg.socialmediagamer.adapters.MyPostsAdapter;
import com.devfg.socialmediagamer.models.Post;
import com.devfg.socialmediagamer.providers.AuthProvider;
import com.devfg.socialmediagamer.providers.PostProvider;
import com.devfg.socialmediagamer.providers.UserProviders;
import com.devfg.socialmediagamer.utils.ViewedMessageHelper;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserProfileActivity extends AppCompatActivity {

    TextView tvProfileUsername, tvProfilePhone, tvProfileEmail, tvProfilePostNumber, tvPostExist;
    ImageView ivProfileCover;
    CircleImageView civProfilePhoto;
    RecyclerView recyclerView;
    Toolbar toolbar;
    FloatingActionButton fabChat;

    UserProviders userProviders;
    AuthProvider authProvider;
    PostProvider postProvider;

    String mExtraIdUser;
    MyPostsAdapter adapter;
    ListenerRegistration mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        tvProfileUsername = findViewById(R.id.textViewProfileUsername);
        tvProfilePhone = findViewById(R.id.textViewProfilePhone);
        tvProfileEmail = findViewById(R.id.textViewProfileEmail);
        tvProfilePostNumber = findViewById(R.id.textViewProfilePostNumber);
        ivProfileCover = findViewById(R.id.imageViewProfileCover);
        civProfilePhoto = findViewById(R.id.circleImageViewProfilePhoto);
        tvPostExist = findViewById(R.id.textViewPostExist);
        recyclerView = findViewById(R.id.recyclerviewMyPost);
        fabChat = findViewById(R.id.fabChat);
        toolbar = findViewById(R.id.toolbarProfile);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(UserProfileActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        userProviders = new UserProviders();
        authProvider = new AuthProvider();
        postProvider = new PostProvider();

        mExtraIdUser = getIntent().getStringExtra("idUser");

        if (authProvider.getUid().equals(mExtraIdUser)) {
            fabChat.setEnabled(false);
        }

        fabChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToChatActivity();
            }
        });

        getUser();
        getPostNumber();
        checkIfExistPost();
    }

    private void goToChatActivity() {
        Intent intent = new Intent(UserProfileActivity.this, ChatActivity.class);
        intent.putExtra("idUser1", authProvider.getUid());
        intent.putExtra("idUser2", mExtraIdUser);
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        Query query = postProvider.getPostByUser(mExtraIdUser);
        FirestoreRecyclerOptions<Post> options = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        adapter = new MyPostsAdapter(options, UserProfileActivity.this);
        recyclerView.setAdapter(adapter);
        adapter.startListening();
        ViewedMessageHelper.updateOnline(true, UserProfileActivity.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ViewedMessageHelper.updateOnline(false, UserProfileActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mListener != null) {
            mListener.remove();
        }
    }

    private void getPostNumber() {
        postProvider.getPostByUser(mExtraIdUser).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                int numberPost = queryDocumentSnapshots.size();
                tvProfilePostNumber.setText(String.valueOf(numberPost));
            }
        });
    }

    private void getUser() {
        userProviders.getUser(mExtraIdUser).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    if (documentSnapshot.contains("email")) {
                        String email = documentSnapshot.getString("email");
                        tvProfileEmail.setText(email);
                    } if (documentSnapshot.contains("phone")) {
                        String phone = documentSnapshot.getString("phone");
                        tvProfilePhone.setText(phone);
                    } if (documentSnapshot.contains("username")) {
                        String username = documentSnapshot.getString("username");
                        tvProfileUsername.setText(username);
                    } if (documentSnapshot.contains("image_profile")) {
                        String imageProfile = documentSnapshot.getString("image_profile");
                        if (imageProfile != null) {
                            if (!imageProfile.isEmpty()) {
                                Picasso.with(UserProfileActivity.this).load(imageProfile).into(civProfilePhoto);
                            }
                        }
                    }  if (documentSnapshot.contains("image_cover")) {
                        String imageCover = documentSnapshot.getString("image_cover");
                        if (imageCover != null) {
                            if (!imageCover.isEmpty()) {
                                Picasso.with(UserProfileActivity.this).load(imageCover).into(ivProfileCover);
                            }
                        }
                    }
                }
            }
        });
    }

    private void checkIfExistPost() {
        mListener = postProvider.getPostByUser(mExtraIdUser).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    int numberPost = queryDocumentSnapshots.size();
                    if (numberPost > 0) {
                        tvPostExist.setText("Publicaciones");
                        tvPostExist.setTextColor(Color.RED);
                    } else {
                        tvPostExist.setText("No hay publicaciones");
                        tvPostExist.setTextColor(Color.GRAY);
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}