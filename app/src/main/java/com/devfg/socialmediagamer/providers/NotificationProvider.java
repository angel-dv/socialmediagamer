package com.devfg.socialmediagamer.providers;

import com.devfg.socialmediagamer.models.FCMBody;
import com.devfg.socialmediagamer.models.FCMResponse;
import com.devfg.socialmediagamer.retrofit.IFCMApi;
import com.devfg.socialmediagamer.retrofit.RetrofitClient;

import retrofit2.Call;

public class NotificationProvider {

    private String url = "https://fcm.googleapis.com/";

    public NotificationProvider() {

    }

    public Call<FCMResponse> sendNotification(FCMBody body) {
        return RetrofitClient.getClient(url).create(IFCMApi.class).send(body);
    }

}
