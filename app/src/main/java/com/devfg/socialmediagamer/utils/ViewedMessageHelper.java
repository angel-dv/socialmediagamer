package com.devfg.socialmediagamer.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;

import com.devfg.socialmediagamer.providers.AuthProvider;
import com.devfg.socialmediagamer.providers.UserProviders;

import java.util.List;

public class ViewedMessageHelper {

    public static void updateOnline(boolean status, final Context context) {
        UserProviders userProviders = new UserProviders();
        AuthProvider authProvider = new AuthProvider();

        if (authProvider.getUid() != null) {
            if (isApplicationSentToBackground(context)) {
                userProviders.updateOnline(authProvider.getUid(), status);
            } else if (status) {
                userProviders.updateOnline(authProvider.getUid(), status);
            }
        }
    }

    public static boolean isApplicationSentToBackground(final Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> task = activityManager.getRunningTasks(1);

        if (!task.isEmpty()) {
            ComponentName topActivity = task.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }

        return false;
    }
}