package com.devfg.socialmediagamer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.devfg.socialmediagamer.R;
import com.devfg.socialmediagamer.activities.FiltersActivity;

public class FilterFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;

    private View v;
    CardView cvPS4, cvXBOX, cvNintendo, cvPC;


    public FilterFragment() {
        // Required empty public constructor
    }

    public static FilterFragment newInstance(String param1) {
        FilterFragment fragment = new FilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_filter, container, false);

        cvPS4 = v.findViewById(R.id.cardViewPS4);
        cvXBOX = v.findViewById(R.id.cardViewXBOX);
        cvNintendo = v.findViewById(R.id.cardViewNINTENDO);
        cvPC = v.findViewById(R.id.cardViewPC);

        cvPS4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToFilterActivity("PS4");
            }
        });

        cvXBOX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToFilterActivity("XBOX");
            }
        });

        cvNintendo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToFilterActivity("NINTENDO");
            }
        });

        cvPC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToFilterActivity("PC");
            }
        });

        return v;
    }

    private void goToFilterActivity(String category) {
        Intent intent = new Intent(getContext(), FiltersActivity.class);
        intent.putExtra("category", category);
        startActivity(intent);
    }
}