package com.devfg.socialmediagamer.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.devfg.socialmediagamer.R;
import com.devfg.socialmediagamer.activities.EditProfileActivity;
import com.devfg.socialmediagamer.adapters.MyPostsAdapter;
import com.devfg.socialmediagamer.models.Post;
import com.devfg.socialmediagamer.providers.AuthProvider;
import com.devfg.socialmediagamer.providers.PostProvider;
import com.devfg.socialmediagamer.providers.UserProviders;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    View v;
    LinearLayout llEditProfile;
    TextView tvProfileUsername, tvProfilePhone, tvProfileEmail, tvProfilePostNumber, tvPostExist;
    ImageView ivProfileCover;
    CircleImageView civProfilePhoto;
    RecyclerView recyclerView;

    UserProviders userProviders;
    AuthProvider authProvider;
    PostProvider postProvider;
    MyPostsAdapter adapter;
    ListenerRegistration mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_profile, container, false);

        llEditProfile = v.findViewById(R.id.linearLayoutEditProfile);
        tvProfileUsername = v.findViewById(R.id.textViewProfileUsername);
        tvProfilePhone = v.findViewById(R.id.textViewProfilePhone);
        tvProfileEmail = v.findViewById(R.id.textViewProfileEmail);
        tvProfilePostNumber = v.findViewById(R.id.textViewProfilePostNumber);
        ivProfileCover = v.findViewById(R.id.imageViewProfileCover);
        civProfilePhoto = v.findViewById(R.id.circleImageViewProfilePhoto);
        tvPostExist = v.findViewById(R.id.textViewPostExist);
        recyclerView = v.findViewById(R.id.recyclerviewMyPost);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        llEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEditProfile();
            }
        });

        userProviders = new UserProviders();
        authProvider = new AuthProvider();
        postProvider = new PostProvider();

        getUser();
        getPostNumber();
        checkIfExistPost();
        return v;
    }

    private void checkIfExistPost() {
        mListener = postProvider.getPostByUser(authProvider.getUid()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null) {
                    int numberPost = queryDocumentSnapshots.size();
                    if (numberPost > 0) {
                        tvPostExist.setText("Publicaciones");
                        tvPostExist.setTextColor(Color.RED);
                    } else {
                        tvPostExist.setText("No hay publicaciones");
                        tvPostExist.setTextColor(Color.GRAY);
                    }
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Query query = postProvider.getPostByUser(authProvider.getUid());
        FirestoreRecyclerOptions<Post> options = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        adapter = new MyPostsAdapter(options, getContext());
        recyclerView.setAdapter(adapter);
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mListener != null) {
            mListener.remove();
        }
    }

    private void goToEditProfile() {
        Intent intent = new Intent(getContext(), EditProfileActivity.class);
        startActivity(intent);
    }

    private void getPostNumber() {
        postProvider.getPostByUser(authProvider.getUid()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                int numberPost = queryDocumentSnapshots.size();
                tvProfilePostNumber.setText(String.valueOf(numberPost));
            }
        });
    }

    private void getUser() {
        userProviders.getUser(authProvider.getUid()).addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    if (documentSnapshot.contains("email")) {
                        String email = documentSnapshot.getString("email");
                        tvProfileEmail.setText(email);
                    } if (documentSnapshot.contains("phone")) {
                        String phone = documentSnapshot.getString("phone");
                        tvProfilePhone.setText(phone);
                    } if (documentSnapshot.contains("username")) {
                        String username = documentSnapshot.getString("username");
                        tvProfileUsername.setText(username);
                    } if (documentSnapshot.contains("image_profile")) {
                        String imageProfile = documentSnapshot.getString("image_profile");
                        if (imageProfile != null) {
                            if (!imageProfile.isEmpty()) {
                                Picasso.with(getContext()).load(imageProfile).into(civProfilePhoto);
                            }
                        }
                    }  if (documentSnapshot.contains("image_cover")) {
                        String imageCover = documentSnapshot.getString("image_cover");
                        if (imageCover != null) {
                            if (!imageCover.isEmpty()) {
                                Picasso.with(getContext()).load(imageCover).into(ivProfileCover);
                            }
                        }
                    }
                }
            }
        });
    }
}