package com.devfg.socialmediagamer.retrofit;

import com.devfg.socialmediagamer.models.FCMBody;
import com.devfg.socialmediagamer.models.FCMResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMApi {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAA98exjOs:APA91bFCkQ4yRNGRFVfg1IpWjXNBXkZdQ9t7NiL_HnM4O4_9oXoLAg8VuEdgFrwxrqOUm5jgn5LEQITGvcDcYZRRHyKzFDdw73sMrG2Mh6__n4Tb7S11oSeSCvw8Ks2o7saryNhUqqfZ"
    })
    @POST("fcm/send")
    Call<FCMResponse> send(@Body FCMBody body);
}
