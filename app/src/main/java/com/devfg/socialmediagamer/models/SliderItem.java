package com.devfg.socialmediagamer.models;

public class SliderItem {

    String imgUrl;
    long timestamp;

    public SliderItem() {}

    public SliderItem(String imgUrl, long timestamp) {
        this.imgUrl = imgUrl;
        this.timestamp = timestamp;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
